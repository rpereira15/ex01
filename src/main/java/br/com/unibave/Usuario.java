package br.com.unibave;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@NamedQueries(
        @NamedQuery(name="usuario.lista", query="SELECT u FROM Usuario u")
)
public class Usuario {

    protected Usuario(){}

    public Usuario(String e, String u){
        this.email = e;
        this.usuario = u;
    }



    @Id
    @GeneratedValue
    private Long codigo;

    @Column(length = 255, nullable = false)
    private String email;

    @Column(length = 10, nullable = false)
    private String usuario;




    @OneToOne
    private Pessoa pessoa;



    @OneToMany
    private List<UsuarioLog> logs = new ArrayList<UsuarioLog>();

    @ManyToMany(mappedBy="usuarios")
    private List<Permissao> permissoes = new ArrayList<Permissao>();

    public Pessoa getPessoa() {
        return pessoa;
    }
    public Long getCodigo() {
        return codigo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }


    public void setLogs(List<UsuarioLog> logs) {
        this.logs = logs;
    }

    public void addPermissao(Permissao p){
        permissoes.add(p);
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public void addLog(UsuarioLog u){
        logs.add(u);
    }

    public List<Permissao> getPermissoes() {
        return Collections.unmodifiableList(permissoes);
    }

    public List<UsuarioLog> getLogs() {
        return Collections.unmodifiableList(logs);
    }

    @Override
    public String toString(){
        return "Aluno{"+"codigo"+codigo+", pessoa="+getPessoa()+", cidade= "+email+"}";
    }
}
