package br.com.unibave;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Permissao {



    protected  Permissao(){}
    public Permissao(String d){
        this.descricao = d;
    }


    @Id
    @GeneratedValue
    private Long codigo;

    @Column(length = 100, nullable = false)
    private String descricao;

    @ManyToMany
    private List<Usuario> usuarios = new ArrayList<Usuario>();


    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


    public void addUsuario(Usuario usuario) {
        this.usuarios.add(usuario);
    }
}
