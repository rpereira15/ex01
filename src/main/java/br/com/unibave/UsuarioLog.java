package br.com.unibave;

import org.h2.store.Data;

import javax.persistence.*;
import java.util.List;

@Entity
public class UsuarioLog {


    @Id
    @GeneratedValue
    private Long codigo;


    @ManyToOne
    private Usuario usuario = new Usuario();

    private String acesso;

    public Long getCodigo() {
        return codigo;
    }

    public String getAcesso() {
        return acesso;
    }

    public void setAcesso(String acesso) {
        this.acesso = acesso;
    }
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

//leonardo.zanivan@gmail.com
}
