package br.com.unibave;

import javax.persistence.*;

@Entity
public class Pessoa {



    public Pessoa(){

    }

    public Pessoa(String n, String sb, Usuario u){
        this.nome = n;
        this.sobrenome = sb;
    }


    @Id
    @GeneratedValue
    private Long codigo;

    @Column(length = 255)
    private String nome;

    @Column(length = 255)
    private String sobrenome;


    @OneToOne(mappedBy="pessoa",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Usuario usuario;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getCodigo() {
        return codigo;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }


    public void addUsuario(Usuario u){
        u.setPessoa(this);
        this.usuario = u;
    }
}
