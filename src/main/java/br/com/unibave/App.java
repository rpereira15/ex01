package br.com.unibave;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Permissao permissao1 = new Permissao("Permissao1");

        Usuario usuario1 = new Usuario("email@mail.com", "user");

        Pessoa pessoa1 = new Pessoa("Nome", "Sobrenome", usuario1);

        UsuarioLog log1 = new UsuarioLog();
        log1.setAcesso("DD-MM-YYYY HH:MM:SS");
        log1.setUsuario(usuario1);

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("work");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx =  em.getTransaction();
        tx.begin();




        permissao1.addUsuario(usuario1);
        usuario1.addPermissao(permissao1);
        usuario1.setPessoa(pessoa1);
        usuario1.addLog(log1);


        em.persist(log1);
        em.persist(permissao1);
        em.persist(usuario1);
        em.persist(pessoa1);


        tx.commit();
        em.close();


        em = emf.createEntityManager();
        List<Usuario> usuario = em.createNamedQuery("usuario.lista", Usuario.class)
                .setMaxResults(10)
                .getResultList();

        usuario.forEach(a-> {
            System.out.println(a);
            a.getPermissoes().forEach(System.out::println);
            a.getLogs().forEach(System.out::println);
        });

        em.close();
        emf.close();

    }
}
